describe ("Tickets",() => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("fills all the text input fields", () => {
        const firstName = "Lucas";
        const lastName = "Rezende";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("lucasrezen@gmail.com");
        cy.get("#requests").type("Vegeterian");
        cy.get("#signature").type(`${firstName} ${lastName}`);    
    });

    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("select vip ticket type", () =>{
        cy.get("#vip").check();
    });

    it("select social media", () =>{
        cy.get("#social-media").check();
    });

    it("selects 'friend', and 'publication', then unchek", () =>{
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heaiding", () =>{
        cy.get("header h1").should("contain","TICKETBOX")
    });

    it("alerts on invali email", () =>{
        cy.get("#email")
          .as("email")
          .type("luca-gmail.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
          .clear()
          .type("luca@gmail.com");

        cy.get("#email.invalid").should("not.exist");  
    });

    it("fill an reset the form", () =>{
        const firstName = "Lucas";
        const lastName = "Rezende";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("lucasrezen@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click();
        cy.get("#signature").type(fullName);
        
        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get("button[type='reset']").click();
        
        cy.get("@submitButton").should("be.disabled");


    });

    it("fills manatory filds using support command", () =>{
       const customer = {
           firstName: "João",
           lastName: "Silva",
           email: "mariasilva@example",
       };

       cy.fillMandatoryFields(customer);

       cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get("#agree").uncheck();
        
        cy.get("@submitButton").should("be.disabled");

    });
});